package com.a.b;

import javax.vecmath.Vector2d;

public class b
{
  private Long a;
  private String b;
  private double c;
  private double d;
  private double e;
  private Vector2d f = new Vector2d(0.0D, 0.0D);
  private double g;
  private Vector2d h = new Vector2d(0.0D, 0.0D);
  private double i;
  private double j;
  private double k;
  private double l;
  private double m = 1.0D;
  private double n;
  private com.a.b.b.b o;
  private boolean p;

  public b()
  {
  }

  public b(Long paramLong, String paramString, double paramDouble1, double paramDouble2, double paramDouble3, Vector2d paramVector2d1, double paramDouble4, Vector2d paramVector2d2, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9, double paramDouble10, com.a.b.b.b paramb, boolean paramBoolean)
  {
    this.a = paramLong;
    this.b = paramString;
    this.c = paramDouble1;
    this.d = paramDouble2;
    this.e = paramDouble3;
    this.f = (paramVector2d1 == null ? null : new Vector2d(paramVector2d1));
    this.g = paramDouble4;
    this.h = (paramVector2d2 == null ? null : new Vector2d(paramVector2d2));
    this.i = paramDouble5;
    this.k = paramDouble6;
    this.l = paramDouble7;
    this.m = paramDouble8;
    this.n = paramDouble9;
    this.j = paramDouble10;
    this.o = (paramb == null ? null : paramb.b());
    this.p = paramBoolean;
  }

  public b(b paramb)
  {
    this(paramb.a, paramb.b, paramb.c, paramb.d, paramb.e, paramb.f, paramb.g, paramb.h, paramb.i, paramb.k, paramb.l, paramb.m, paramb.n, paramb.j, paramb.o, paramb.p);
  }

  public Long a()
  {
    return this.a;
  }

  public void a(Long paramLong)
  {
    this.a = paramLong;
  }

  public String b()
  {
    return this.b;
  }

  public void a(String paramString)
  {
    this.b = paramString;
  }

  public double c()
  {
    return this.c;
  }

  public void a(double paramDouble)
  {
    this.c = paramDouble;
  }

  public double d()
  {
    return this.d;
  }

  public void b(double paramDouble)
  {
    this.d = paramDouble;
  }

  public double e()
  {
    return this.e;
  }

  public void c(double paramDouble)
  {
    while (paramDouble > 3.141592653589793D)
      paramDouble -= 6.283185307179586D;
    while (paramDouble < -3.141592653589793D)
      paramDouble += 6.283185307179586D;
    this.e = paramDouble;
  }

  public Vector2d f()
  {
    return this.f;
  }

  public void a(Vector2d paramVector2d)
  {
    this.f = paramVector2d;
  }

  public double g()
  {
    return this.g;
  }

  public Vector2d h()
  {
    return this.h;
  }

  public double i()
  {
    return this.i;
  }

  public void d(double paramDouble)
  {
    this.i = paramDouble;
  }

  public double j()
  {
    return this.k;
  }

  public void e(double paramDouble)
  {
    this.k = paramDouble;
  }

  public double k()
  {
    return this.l;
  }

  public void f(double paramDouble)
  {
    this.l = paramDouble;
  }

  public double l()
  {
    return this.m;
  }

  public void g(double paramDouble)
  {
    this.m = paramDouble;
  }

  public double m()
  {
    return this.n;
  }

  public void h(double paramDouble)
  {
    this.n = paramDouble;
  }

  public double n()
  {
    return this.j;
  }

  public void i(double paramDouble)
  {
    this.j = paramDouble;
  }

  public com.a.b.b.b o()
  {
    return this.o;
  }

  public void a(com.a.b.b.b paramb)
  {
    this.o = paramb;
  }

  public boolean p()
  {
    return this.p;
  }

  public void a(boolean paramBoolean)
  {
    this.p = paramBoolean;
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.b.b
 * JD-Core Version:    0.6.2
 */