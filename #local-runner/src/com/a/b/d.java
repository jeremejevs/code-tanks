package com.a.b;

import javax.vecmath.Vector2d;

public final class d
{
  private final b a;
  private final b b;
  private final Vector2d c;
  private final Vector2d d;

  public d(b paramb1, b paramb2, Vector2d paramVector2d1, Vector2d paramVector2d2)
  {
    this.a = paramb1;
    this.b = paramb2;
    this.c = paramVector2d1;
    this.d = paramVector2d2;
  }

  public b a()
  {
    return this.a;
  }

  public b b()
  {
    return this.b;
  }

  public Vector2d c()
  {
    return this.c;
  }

  public Vector2d d()
  {
    return this.d;
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.b.d
 * JD-Core Version:    0.6.2
 */