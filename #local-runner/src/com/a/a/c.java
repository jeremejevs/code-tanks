package com.a.a;

import javax.vecmath.Vector2d;

public final class c
{
  private final g a;
  private final d b;
  private final d c;
  private final Vector2d d;
  private final Vector2d e;

  public c(g paramg, d paramd1, d paramd2, Vector2d paramVector2d1, Vector2d paramVector2d2)
  {
    this.a = paramg;
    this.b = paramd1;
    this.c = paramd2;
    this.d = paramVector2d1;
    this.e = paramVector2d2;
  }

  public g a()
  {
    return this.a;
  }

  public d b()
  {
    return this.b;
  }

  public d c()
  {
    return this.c;
  }

  public Vector2d d()
  {
    return this.d;
  }

  public Vector2d e()
  {
    return this.e;
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.c
 * JD-Core Version:    0.6.2
 */