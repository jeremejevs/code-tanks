package com.a.a.a.a.b.b;

import com.a.a.a.a.a.j;
import com.a.a.a.a.b.c.c.b;
import java.util.Map;

public final class a
{
  private a()
  {
    throw new UnsupportedOperationException();
  }

  public static j a(com.a.a.a.a.b.e.a parama)
  {
    Map localMap = parama.i();
    if (localMap.isEmpty())
      return new j(parama.a(), parama.b(), parama.c(), parama.d(), parama.e(), parama.f());
    return new j(parama.a(), parama.b(), parama.c(), parama.d(), parama.e(), parama.f(), localMap);
  }

  public static com.a.a.a.a.a.c a(com.a.a.a.a.b.c.c.a parama)
  {
    if ((parama instanceof com.a.a.a.a.b.c.c.c))
      return com.a.a.a.a.a.c.a;
    if ((parama instanceof b))
      return com.a.a.a.a.a.c.b;
    throw new IllegalArgumentException("Unsupported shell class: " + parama.getClass() + '.');
  }

  public static com.a.a.a.a.a.c b(com.a.a.a.a.b.c.c.a parama)
  {
    if ((parama instanceof com.a.a.a.a.b.c.c.c))
      return com.a.a.a.a.a.c.c;
    if ((parama instanceof b))
      return com.a.a.a.a.a.c.d;
    throw new IllegalArgumentException("Unsupported shell class: " + parama.getClass() + '.');
  }

  public static com.a.a.a.a.a.c c(com.a.a.a.a.b.c.c.a parama)
  {
    if ((parama instanceof com.a.a.a.a.b.c.c.c))
      return com.a.a.a.a.a.c.e;
    if ((parama instanceof b))
      return com.a.a.a.a.a.c.f;
    throw new IllegalArgumentException("Unsupported shell class: " + parama.getClass() + '.');
  }

  public static com.a.a.a.a.a.c d(com.a.a.a.a.b.c.c.a parama)
  {
    if ((parama instanceof com.a.a.a.a.b.c.c.c))
      return com.a.a.a.a.a.c.g;
    if ((parama instanceof b))
      return com.a.a.a.a.a.c.h;
    throw new IllegalArgumentException("Unsupported shell class: " + parama.getClass() + '.');
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.a.a.b.b.a
 * JD-Core Version:    0.6.2
 */