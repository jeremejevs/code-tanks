package com.a.a.a.a.b.b;

import com.a.a.a.a.a.a;
import com.a.b.b.c;

public final class e
{
  private e()
  {
    throw new UnsupportedOperationException();
  }

  public static a a(com.a.a.a.a.b.c.e.b paramb)
  {
    com.a.b.b.b localb = paramb.d().o();
    if (!(localb instanceof c))
      throw new IllegalArgumentException("Unsupported obstacle form: " + localb + '.');
    c localc = (c)localb;
    return new a(paramb.c(), localc.a(), localc.c(), paramb.d().c(), paramb.d().d());
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.a.a.b.b.e
 * JD-Core Version:    0.6.2
 */