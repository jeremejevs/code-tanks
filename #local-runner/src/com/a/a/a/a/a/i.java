package com.a.a.a.a.a;

public final class i
{
  private final long a;
  private final String b;
  private final int c;
  private final boolean d;

  public i(long paramLong, String paramString, int paramInt, boolean paramBoolean)
  {
    this.a = paramLong;
    this.b = paramString;
    this.c = paramInt;
    this.d = paramBoolean;
  }

  public String a()
  {
    return this.b;
  }

  public int b()
  {
    return this.c;
  }

  public boolean c()
  {
    return this.d;
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.a.a.a.i
 * JD-Core Version:    0.6.2
 */