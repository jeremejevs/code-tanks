package com.a.a.a.a.a;

import java.util.HashMap;
import java.util.Map;

public final class j
{
  private final long a;
  private final double b;
  private final double c;
  private final double d;
  private final c e;
  private final int f;
  private final Map g;

  public j(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, c paramc, int paramInt)
  {
    this.a = paramLong;
    this.b = paramDouble1;
    this.c = paramDouble2;
    this.d = paramDouble3;
    this.e = paramc;
    this.f = paramInt;
    this.g = null;
  }

  public j(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, c paramc, int paramInt, Map paramMap)
  {
    this.a = paramLong;
    this.b = paramDouble1;
    this.c = paramDouble2;
    this.d = paramDouble3;
    this.e = paramc;
    this.f = paramInt;
    this.g = new HashMap(paramMap);
  }

  public double a()
  {
    return this.b;
  }

  public double b()
  {
    return this.c;
  }

  public c c()
  {
    return this.e;
  }

  public int d()
  {
    return this.f;
  }

  public Object a(String paramString)
  {
    return this.g == null ? null : this.g.get(paramString);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.a.a.a.j
 * JD-Core Version:    0.6.2
 */