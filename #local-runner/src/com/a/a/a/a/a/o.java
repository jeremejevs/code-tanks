package com.a.a.a.a.a;

import com.a.a.a.a.b.b.i;
import com.google.gson.annotations.Until;

public final class o extends l
{
  private final long a;

  @Until(0.0D)
  private final String b;
  private final int c;
  private final double d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;

  @Until(0.0D)
  private final boolean l;
  private final b m;

  public o(long paramLong1, long paramLong2, String paramString, int paramInt1, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean, b paramb)
  {
    super(paramLong1, paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramDouble5, paramDouble6, paramDouble7, paramDouble8);
    this.a = paramLong2;
    this.b = paramString;
    this.c = paramInt1;
    this.d = paramDouble9;
    this.e = paramInt2;
    this.f = paramInt3;
    this.g = paramInt4;
    this.h = paramInt5;
    this.i = paramInt6;
    this.j = paramInt7;
    this.k = paramInt8;
    this.l = paramBoolean;
    this.m = paramb;
  }

  public String a()
  {
    return this.b;
  }

  public int b()
  {
    return this.c;
  }

  public double l()
  {
    return this.d;
  }

  public int m()
  {
    return this.e;
  }

  public int n()
  {
    return this.f;
  }

  public int o()
  {
    return this.g;
  }

  public int p()
  {
    return this.h;
  }

  public int q()
  {
    return this.i;
  }

  public int r()
  {
    return this.j;
  }

  public int s()
  {
    return this.k;
  }

  public boolean t()
  {
    return this.l;
  }

  public b u()
  {
    return this.m;
  }

  public double c(double paramDouble1, double paramDouble2)
  {
    double d1 = i.a(f(), g(), paramDouble1, paramDouble2);
    for (double d2 = d1 - j() - this.d; d2 > 3.141592653589793D; d2 -= 6.283185307179586D);
    while (d2 < -3.141592653589793D)
      d2 += 6.283185307179586D;
    return d2;
  }

  public double c(l paraml)
  {
    return c(paraml.f(), paraml.g());
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.a.a.a.o
 * JD-Core Version:    0.6.2
 */