package com.a.a.a.a.a;

import java.util.Arrays;

public final class h
{
  private final o[] a;
  private final n b;

  public h(o[] paramArrayOfo, n paramn)
  {
    this.a = ((o[])Arrays.copyOf(paramArrayOfo, paramArrayOfo.length));
    this.b = paramn;
  }

  public o[] a()
  {
    return (o[])Arrays.copyOf(this.a, this.a.length);
  }

  public n b()
  {
    return this.b;
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.a.a.a.h
 * JD-Core Version:    0.6.2
 */