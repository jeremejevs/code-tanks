package com.a.a.a.a.a;

import com.a.a.a.a.b.b.i;
import com.google.gson.annotations.Until;

public abstract class l
{
  private final long a;

  @Until(0.0D)
  private final double b;

  @Until(0.0D)
  private final double c;
  private final double d;
  private final double e;

  @Until(0.0D)
  private final double f;

  @Until(0.0D)
  private final double g;
  private final double h;
  private final double i;

  protected l(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8)
  {
    this.a = paramLong;
    this.b = paramDouble1;
    this.c = paramDouble2;
    this.d = paramDouble3;
    this.e = paramDouble4;
    this.f = paramDouble5;
    this.g = paramDouble6;
    this.h = paramDouble7;
    this.i = paramDouble8;
  }

  public long c()
  {
    return this.a;
  }

  public double d()
  {
    return this.b;
  }

  public double e()
  {
    return this.c;
  }

  public final double f()
  {
    return this.d;
  }

  public final double g()
  {
    return this.e;
  }

  public final double h()
  {
    return this.f;
  }

  public final double i()
  {
    return this.g;
  }

  public final double j()
  {
    return this.h;
  }

  public double k()
  {
    return this.i;
  }

  public double a(double paramDouble1, double paramDouble2)
  {
    double d1 = i.a(this.d, this.e, paramDouble1, paramDouble2);
    for (double d2 = d1 - this.h; d2 > 3.141592653589793D; d2 -= 6.283185307179586D);
    while (d2 < -3.141592653589793D)
      d2 += 6.283185307179586D;
    return d2;
  }

  public double a(l paraml)
  {
    return a(paraml.d, paraml.e);
  }

  public double b(double paramDouble1, double paramDouble2)
  {
    return StrictMath.hypot(paramDouble1 - this.d, paramDouble2 - this.e);
  }

  public double b(l paraml)
  {
    return b(paraml.d, paraml.e);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.a.a.a.l
 * JD-Core Version:    0.6.2
 */