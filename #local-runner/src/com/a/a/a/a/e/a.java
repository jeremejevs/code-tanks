package com.a.a.a.a.e;

import com.a.a.g;
import com.a.b.c;
import com.google.inject.AbstractModule;
import com.google.inject.binder.AnnotatedBindingBuilder;
import javax.vecmath.Vector2d;

public final class a extends AbstractModule
{
  protected void configure()
  {
    bind(com.a.a.b.class).toInstance(new com.a.a.a.a.b.b());
    bind(c.class).toInstance(new com.a.b.a.a.b(new Vector2d(0.0D, 0.0D), 10));
    bind(g.class).toInstance(new com.a.a.a.a.b.a());
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.a.a.a.a.e.a
 * JD-Core Version:    0.6.2
 */