package com.google.inject;

public abstract interface Provider extends javax.inject.Provider
{
  public abstract Object get();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.Provider
 * JD-Core Version:    0.6.2
 */