package com.google.inject.internal;

abstract interface Initializable
{
  public abstract Object get(Errors paramErrors)
    throws ErrorsException;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.Initializable
 * JD-Core Version:    0.6.2
 */