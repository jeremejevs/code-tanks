package com.google.inject.internal.util;

public abstract interface $FinalizableReference
{
  public abstract void finalizeReferent();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.util..FinalizableReference
 * JD-Core Version:    0.6.2
 */