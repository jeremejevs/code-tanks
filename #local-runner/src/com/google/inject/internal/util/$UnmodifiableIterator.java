package com.google.inject.internal.util;

import java.util.Iterator;

public abstract class $UnmodifiableIterator
  implements Iterator
{
  public final void remove()
  {
    throw new UnsupportedOperationException();
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.util..UnmodifiableIterator
 * JD-Core Version:    0.6.2
 */