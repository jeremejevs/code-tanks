package com.google.inject.internal.util;

public abstract interface $Function
{
  public abstract Object apply(@$Nullable Object paramObject);

  public abstract boolean equals(@$Nullable Object paramObject);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.util..Function
 * JD-Core Version:    0.6.2
 */