package com.google.inject.internal.cglib.proxy;

import java.lang.reflect.Method;

public abstract interface $MethodInterceptor extends $Callback
{
  public abstract Object intercept(Object paramObject, Method paramMethod, Object[] paramArrayOfObject, .MethodProxy paramMethodProxy)
    throws Throwable;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.proxy..MethodInterceptor
 * JD-Core Version:    0.6.2
 */