package com.google.inject.internal.cglib.proxy;

import java.lang.reflect.Method;

public abstract interface $CallbackFilter
{
  public abstract int accept(Method paramMethod);

  public abstract boolean equals(Object paramObject);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.proxy..CallbackFilter
 * JD-Core Version:    0.6.2
 */