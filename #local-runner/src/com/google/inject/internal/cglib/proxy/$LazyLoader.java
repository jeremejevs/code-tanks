package com.google.inject.internal.cglib.proxy;

public abstract interface $LazyLoader extends $Callback
{
  public abstract Object loadObject()
    throws Exception;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.proxy..LazyLoader
 * JD-Core Version:    0.6.2
 */