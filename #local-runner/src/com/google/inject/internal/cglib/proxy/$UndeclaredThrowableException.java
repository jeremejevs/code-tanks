package com.google.inject.internal.cglib.proxy;

import com.google.inject.internal.cglib.core..CodeGenerationException;

public class $UndeclaredThrowableException extends $CodeGenerationException
{
  public $UndeclaredThrowableException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }

  public Throwable getUndeclaredThrowable()
  {
    return getCause();
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.proxy..UndeclaredThrowableException
 * JD-Core Version:    0.6.2
 */