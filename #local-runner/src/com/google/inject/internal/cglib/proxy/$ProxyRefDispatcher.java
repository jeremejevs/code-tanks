package com.google.inject.internal.cglib.proxy;

public abstract interface $ProxyRefDispatcher extends $Callback
{
  public abstract Object loadObject(Object paramObject)
    throws Exception;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.proxy..ProxyRefDispatcher
 * JD-Core Version:    0.6.2
 */