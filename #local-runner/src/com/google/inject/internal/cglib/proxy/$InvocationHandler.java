package com.google.inject.internal.cglib.proxy;

import java.lang.reflect.Method;

public abstract interface $InvocationHandler extends $Callback
{
  public abstract Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
    throws Throwable;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.proxy..InvocationHandler
 * JD-Core Version:    0.6.2
 */