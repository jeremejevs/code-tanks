package com.google.inject.internal.cglib.proxy;

public abstract interface $FixedValue extends $Callback
{
  public abstract Object loadObject()
    throws Exception;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.proxy..FixedValue
 * JD-Core Version:    0.6.2
 */