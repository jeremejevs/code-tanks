package com.google.inject.internal.cglib.core;

import com.google.inject.internal.asm..Type;

public class $Local
{
  private $Type type;
  private int index;

  public $Local(int paramInt, .Type paramType)
  {
    this.type = paramType;
    this.index = paramInt;
  }

  public int getIndex()
  {
    return this.index;
  }

  public .Type getType()
  {
    return this.type;
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.core..Local
 * JD-Core Version:    0.6.2
 */