package com.google.inject.internal.cglib.core;

public abstract interface $GeneratorStrategy
{
  public abstract byte[] generate($ClassGenerator paramClassGenerator)
    throws Exception;

  public abstract boolean equals(Object paramObject);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.core..GeneratorStrategy
 * JD-Core Version:    0.6.2
 */