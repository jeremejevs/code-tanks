package com.google.inject.internal.cglib.core;

import com.google.inject.internal.asm..Type;

public abstract interface $Customizer
{
  public abstract void customize($CodeEmitter paramCodeEmitter, .Type paramType);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.core..Customizer
 * JD-Core Version:    0.6.2
 */