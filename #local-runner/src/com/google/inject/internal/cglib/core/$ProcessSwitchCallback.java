package com.google.inject.internal.cglib.core;

import com.google.inject.internal.asm..Label;

public abstract interface $ProcessSwitchCallback
{
  public abstract void processCase(int paramInt, .Label paramLabel)
    throws Exception;

  public abstract void processDefault()
    throws Exception;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.core..ProcessSwitchCallback
 * JD-Core Version:    0.6.2
 */