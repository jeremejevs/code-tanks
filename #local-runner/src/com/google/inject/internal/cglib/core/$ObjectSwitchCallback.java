package com.google.inject.internal.cglib.core;

import com.google.inject.internal.asm..Label;

public abstract interface $ObjectSwitchCallback
{
  public abstract void processCase(Object paramObject, .Label paramLabel)
    throws Exception;

  public abstract void processDefault()
    throws Exception;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.core..ObjectSwitchCallback
 * JD-Core Version:    0.6.2
 */