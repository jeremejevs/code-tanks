package com.google.inject.internal.cglib.core;

import com.google.inject.internal.asm..ClassVisitor;

public abstract interface $ClassGenerator
{
  public abstract void generateClass($ClassVisitor paramClassVisitor)
    throws Exception;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.core..ClassGenerator
 * JD-Core Version:    0.6.2
 */