package com.google.inject.internal.cglib.core;

import com.google.inject.internal.asm..Type;

public abstract interface $ProcessArrayCallback
{
  public abstract void processElement($Type paramType);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.cglib.core..ProcessArrayCallback
 * JD-Core Version:    0.6.2
 */