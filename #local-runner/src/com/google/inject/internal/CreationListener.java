package com.google.inject.internal;

abstract interface CreationListener
{
  public abstract void notify(Errors paramErrors);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.CreationListener
 * JD-Core Version:    0.6.2
 */