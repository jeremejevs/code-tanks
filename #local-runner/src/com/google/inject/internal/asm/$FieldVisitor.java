package com.google.inject.internal.asm;

public abstract interface $FieldVisitor
{
  public abstract $AnnotationVisitor visitAnnotation(String paramString, boolean paramBoolean);

  public abstract void visitAttribute($Attribute paramAttribute);

  public abstract void visitEnd();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.internal.asm..FieldVisitor
 * JD-Core Version:    0.6.2
 */