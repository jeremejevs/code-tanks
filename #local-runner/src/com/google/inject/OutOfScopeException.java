package com.google.inject;

public final class OutOfScopeException extends RuntimeException
{
  public OutOfScopeException(String paramString)
  {
    super(paramString);
  }

  public OutOfScopeException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public OutOfScopeException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.OutOfScopeException
 * JD-Core Version:    0.6.2
 */