package com.google.inject.spi;

public abstract interface InjectionListener
{
  public abstract void afterInjection(Object paramObject);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.spi.InjectionListener
 * JD-Core Version:    0.6.2
 */