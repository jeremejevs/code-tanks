package com.google.inject.spi;

import com.google.inject.Provider;

public abstract interface ProviderWithDependencies extends Provider, HasDependencies
{
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.spi.ProviderWithDependencies
 * JD-Core Version:    0.6.2
 */