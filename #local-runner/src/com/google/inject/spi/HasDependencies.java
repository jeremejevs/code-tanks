package com.google.inject.spi;

import java.util.Set;

public abstract interface HasDependencies
{
  public abstract Set getDependencies();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.inject.spi.HasDependencies
 * JD-Core Version:    0.6.2
 */