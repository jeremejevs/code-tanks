package com.google.gson;

public final class JsonIOException extends JsonParseException
{
  private static final long serialVersionUID = 1L;

  public JsonIOException(String paramString)
  {
    super(paramString);
  }

  public JsonIOException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public JsonIOException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.gson.JsonIOException
 * JD-Core Version:    0.6.2
 */