package com.google.gson;

public class JsonParseException extends RuntimeException
{
  static final long serialVersionUID = -4086729973971783390L;

  public JsonParseException(String paramString)
  {
    super(paramString);
  }

  public JsonParseException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public JsonParseException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.gson.JsonParseException
 * JD-Core Version:    0.6.2
 */