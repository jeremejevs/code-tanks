package com.google.gson.internal;

public abstract interface ObjectConstructor
{
  public abstract Object construct();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.gson.internal.ObjectConstructor
 * JD-Core Version:    0.6.2
 */