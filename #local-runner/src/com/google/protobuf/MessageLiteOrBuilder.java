package com.google.protobuf;

public abstract interface MessageLiteOrBuilder
{
  public abstract MessageLite getDefaultInstanceForType();

  public abstract boolean isInitialized();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.protobuf.MessageLiteOrBuilder
 * JD-Core Version:    0.6.2
 */