package com.google.common.collect;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(emulated=true)
abstract class ForwardingImmutableList extends ImmutableList
{
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.common.collect.ForwardingImmutableList
 * JD-Core Version:    0.6.2
 */