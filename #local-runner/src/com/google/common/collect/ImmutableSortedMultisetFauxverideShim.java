package com.google.common.collect;

abstract class ImmutableSortedMultisetFauxverideShim extends ImmutableMultiset
{
  @Deprecated
  public static ImmutableSortedMultiset.Builder builder()
  {
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public static ImmutableSortedMultiset of(Object paramObject)
  {
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public static ImmutableSortedMultiset of(Object paramObject1, Object paramObject2)
  {
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public static ImmutableSortedMultiset of(Object paramObject1, Object paramObject2, Object paramObject3)
  {
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public static ImmutableSortedMultiset of(Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4)
  {
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public static ImmutableSortedMultiset of(Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, Object paramObject5)
  {
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public static ImmutableSortedMultiset of(Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4, Object paramObject5, Object paramObject6, Object[] paramArrayOfObject)
  {
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public static ImmutableSortedMultiset copyOf(Object[] paramArrayOfObject)
  {
    throw new UnsupportedOperationException();
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.common.collect.ImmutableSortedMultisetFauxverideShim
 * JD-Core Version:    0.6.2
 */