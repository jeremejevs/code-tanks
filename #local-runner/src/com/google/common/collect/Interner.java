package com.google.common.collect;

import com.google.common.annotations.Beta;

@Beta
public abstract interface Interner
{
  public abstract Object intern(Object paramObject);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.common.collect.Interner
 * JD-Core Version:    0.6.2
 */