package com.google.common.collect;

@Deprecated
public class AsynchronousComputationException extends ComputationException
{
  private static final long serialVersionUID = 0L;

  public AsynchronousComputationException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.common.collect.AsynchronousComputationException
 * JD-Core Version:    0.6.2
 */