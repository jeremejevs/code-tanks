package com.google.common.io;

import com.google.common.annotations.Beta;
import java.io.OutputStream;

@Beta
public final class NullOutputStream extends OutputStream
{
  public void write(int paramInt)
  {
  }

  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.common.io.NullOutputStream
 * JD-Core Version:    0.6.2
 */