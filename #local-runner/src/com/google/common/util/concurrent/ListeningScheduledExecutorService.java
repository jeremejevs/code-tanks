package com.google.common.util.concurrent;

import com.google.common.annotations.Beta;
import java.util.concurrent.ScheduledExecutorService;

@Beta
public abstract interface ListeningScheduledExecutorService extends ListeningExecutorService, ScheduledExecutorService
{
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     com.google.common.util.concurrent.ListeningScheduledExecutorService
 * JD-Core Version:    0.6.2
 */