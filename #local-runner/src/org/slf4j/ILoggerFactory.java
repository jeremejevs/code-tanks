package org.slf4j;

public abstract interface ILoggerFactory
{
  public abstract Logger getLogger(String paramString);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.slf4j.ILoggerFactory
 * JD-Core Version:    0.6.2
 */