package org.apache.log4j.spi;

import org.apache.log4j.or.ObjectRenderer;

public abstract interface RendererSupport
{
  public abstract void setRenderer(Class paramClass, ObjectRenderer paramObjectRenderer);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.log4j.spi.RendererSupport
 * JD-Core Version:    0.6.2
 */