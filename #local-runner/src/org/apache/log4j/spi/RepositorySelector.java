package org.apache.log4j.spi;

public abstract interface RepositorySelector
{
  public abstract LoggerRepository getLoggerRepository();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.log4j.spi.RepositorySelector
 * JD-Core Version:    0.6.2
 */