package org.apache.http;

public abstract interface HttpRequest extends HttpMessage
{
  public abstract RequestLine getRequestLine();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.HttpRequest
 * JD-Core Version:    0.6.2
 */