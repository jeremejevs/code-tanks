package org.apache.http.conn.ssl;

public class SSLInitializationException extends IllegalStateException
{
  public SSLInitializationException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.conn.ssl.SSLInitializationException
 * JD-Core Version:    0.6.2
 */