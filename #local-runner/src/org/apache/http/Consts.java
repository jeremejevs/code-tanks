package org.apache.http;

import java.nio.charset.Charset;

public final class Consts
{
  public static final Charset UTF_8 = Charset.forName("UTF-8");
  public static final Charset ASCII = Charset.forName("US-ASCII");
  public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.Consts
 * JD-Core Version:    0.6.2
 */