package org.apache.http;

import java.io.IOException;

public class NoHttpResponseException extends IOException
{
  public NoHttpResponseException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.NoHttpResponseException
 * JD-Core Version:    0.6.2
 */