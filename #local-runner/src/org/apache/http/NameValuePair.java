package org.apache.http;

public abstract interface NameValuePair
{
  public abstract String getName();

  public abstract String getValue();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.NameValuePair
 * JD-Core Version:    0.6.2
 */