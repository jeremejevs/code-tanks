package org.apache.http.impl.auth;

public class UnsupportedDigestAlgorithmException extends RuntimeException
{
  public UnsupportedDigestAlgorithmException()
  {
  }

  public UnsupportedDigestAlgorithmException(String paramString)
  {
    super(paramString);
  }

  public UnsupportedDigestAlgorithmException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.impl.auth.UnsupportedDigestAlgorithmException
 * JD-Core Version:    0.6.2
 */