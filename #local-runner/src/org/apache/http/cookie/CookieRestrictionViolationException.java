package org.apache.http.cookie;

public class CookieRestrictionViolationException extends MalformedCookieException
{
  public CookieRestrictionViolationException()
  {
  }

  public CookieRestrictionViolationException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.cookie.CookieRestrictionViolationException
 * JD-Core Version:    0.6.2
 */