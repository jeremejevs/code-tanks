package org.apache.http;

import java.io.IOException;

public class ConnectionClosedException extends IOException
{
  public ConnectionClosedException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.ConnectionClosedException
 * JD-Core Version:    0.6.2
 */