package org.apache.http.io;

public abstract interface EofSensor
{
  public abstract boolean isEof();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.io.EofSensor
 * JD-Core Version:    0.6.2
 */