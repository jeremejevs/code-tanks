package org.apache.http.protocol;

import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponseInterceptor;

public abstract interface HttpProcessor extends HttpRequestInterceptor, HttpResponseInterceptor
{
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.protocol.HttpProcessor
 * JD-Core Version:    0.6.2
 */