package org.apache.http;

public class ProtocolException extends HttpException
{
  public ProtocolException()
  {
  }

  public ProtocolException(String paramString)
  {
    super(paramString);
  }

  public ProtocolException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.ProtocolException
 * JD-Core Version:    0.6.2
 */