package org.apache.http.auth;

public class InvalidCredentialsException extends AuthenticationException
{
  public InvalidCredentialsException()
  {
  }

  public InvalidCredentialsException(String paramString)
  {
    super(paramString);
  }

  public InvalidCredentialsException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.auth.InvalidCredentialsException
 * JD-Core Version:    0.6.2
 */