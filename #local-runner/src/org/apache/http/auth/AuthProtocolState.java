package org.apache.http.auth;

public enum AuthProtocolState
{
  UNCHALLENGED, CHALLENGED, HANDSHAKE, FAILURE, SUCCESS;
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.auth.AuthProtocolState
 * JD-Core Version:    0.6.2
 */