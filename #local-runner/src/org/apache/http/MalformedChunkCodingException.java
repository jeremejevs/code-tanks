package org.apache.http;

import java.io.IOException;

public class MalformedChunkCodingException extends IOException
{
  public MalformedChunkCodingException()
  {
  }

  public MalformedChunkCodingException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     org.apache.http.MalformedChunkCodingException
 * JD-Core Version:    0.6.2
 */