package javax.inject;

public abstract interface Provider
{
  public abstract Object get();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     javax.inject.Provider
 * JD-Core Version:    0.6.2
 */