package net.phys2d.raw.forcesource;

import net.phys2d.raw.Body;

public abstract interface ForceSource
{
  public abstract void apply(Body paramBody, float paramFloat);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     net.phys2d.raw.forcesource.ForceSource
 * JD-Core Version:    0.6.2
 */