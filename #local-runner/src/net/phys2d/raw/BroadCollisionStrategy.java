package net.phys2d.raw;

public abstract interface BroadCollisionStrategy
{
  public abstract void collideBodies(CollisionContext paramCollisionContext, BodyList paramBodyList, float paramFloat);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     net.phys2d.raw.BroadCollisionStrategy
 * JD-Core Version:    0.6.2
 */