package net.phys2d.raw.shapes;

public abstract interface Shape
{
  public abstract AABox getBounds();

  public abstract float getSurfaceFactor();
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     net.phys2d.raw.shapes.Shape
 * JD-Core Version:    0.6.2
 */