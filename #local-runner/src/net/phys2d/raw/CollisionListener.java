package net.phys2d.raw;

public abstract interface CollisionListener
{
  public abstract void collisionOccured(CollisionEvent paramCollisionEvent);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     net.phys2d.raw.CollisionListener
 * JD-Core Version:    0.6.2
 */