package net.phys2d.raw;

public abstract interface CollisionContext
{
  public abstract void resolve(BodyList paramBodyList, float paramFloat);
}

/* Location:           D:\stuff\work\random\CodeTanks\#local-runner\local-runner\
 * Qualified Name:     net.phys2d.raw.CollisionContext
 * JD-Core Version:    0.6.2
 */